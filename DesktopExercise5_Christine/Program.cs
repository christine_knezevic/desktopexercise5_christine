﻿using System;
using System.Collections.Generic;
using Desktop_Exercise_5.Models;
using Desktop_Exercise_5.Utility;

namespace Desktop_Exercise_5
{
  class Program
  {
    static void Main(string[] args)
    {
      var Volleyball = new Sport(Lookups.SportCategory.Team, 6, "Volleyball");

      Volleyball.Length = 60;
      Volleyball.Width = 30;

      Volleyball.WriteSportProperties();

      var Basketball = new Sport(Lookups.SportCategory.Team, 5, "Basketball");
      Basketball.Length = 94;
      Basketball.Width = 50;

      Basketball.WriteSportProperties();

      //var TrackField = new Sport(Lookups.SportCategory.Single, 1, "Track and Field");

      //TrackField.WriteSportProperties();
      
      Console.ReadKey();


    }
  }
}
