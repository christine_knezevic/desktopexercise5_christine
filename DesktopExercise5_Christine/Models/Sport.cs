﻿using Desktop_Exercise_5.Interfaces;
using Desktop_Exercise_5.Utility;
using System;

namespace Desktop_Exercise_5.Models
{
  class Sport: ISport
  {
    private Lookups.SportCategory _SportCategory;
    private string _SportName;
    
    public Sport(Lookups.SportCategory category, int numPlayers, string name)
    {
      this._SportCategory = category;
      this._SportName = name;
      NumberOfPlayers = numPlayers;
    }
    public Lookups.SportCategory SportCategory
    {
      get { return _SportCategory; }
    }

    public int NumberOfPlayers {get; set;}


    public string SportName
    {
      get { return _SportName; }
    }

    public void WriteSportProperties()
    {
      Console.WriteLine("Sport Properties");
      Console.WriteLine("Sport Name: {0}", SportName);
      Console.WriteLine("Category: {0}", SportCategory);
      Console.WriteLine("# of Players: {0}", NumberOfPlayers);
      Console.WriteLine("Court dimensions {0} x {1}", Length, Width);
      Console.WriteLine("\r\n");
    }

    public decimal Length {get; set;}


    public decimal Width { get; set;  }


  }
}
