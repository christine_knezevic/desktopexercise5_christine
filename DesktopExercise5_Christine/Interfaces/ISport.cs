﻿using Desktop_Exercise_5.Utility;

namespace Desktop_Exercise_5.Interfaces
{
  public interface ISport : IBall, ICourt
  {
    /// <summary>
    /// Single, Team, Unknown
    /// </summary>
    Lookups.SportCategory SportCategory { get; }
  }
}
